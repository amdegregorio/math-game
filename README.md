# Math Game Backend

This is the example back end API code for the blog post [Spring Boot with Nuxt](https://amydegregorio.com/2021/10/09/spring-boot-with-nuxt/)

## To run

```
> mvn spring-boot:run
```

Once running, the application is available at http://localhost:8080

### License

This project is licensed under the Apache License version 2.0.  
See the LICENSE file or go to [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) for more information. 
