package com.amydegregorio.mathgame.service;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.amydegregorio.mathgame.dto.Problem;
import com.amydegregorio.mathgame.dto.Problem.Operation;

@Service
public class ProblemService {
   Logger log = LoggerFactory.getLogger(ProblemService.class);

   public Problem generateProblem() {
      log.debug("Entering generateProblem()");
      Random rand = new Random();
      Problem problem = new Problem();
      
      int operator = rand.nextInt(3);
      log.debug("Operator int: " + operator);
      switch(operator) {
          case 0:
             problem.setOperation(Operation.ADD);
             problem.setNumberOne(rand.nextInt(100));
             problem.setNumberTwo(rand.nextInt(100));
             break;
          case 1:
             problem.setOperation(Operation.SUBTRACT);
             problem.setNumberOne(rand.nextInt(100));
             problem.setNumberTwo(rand.nextInt(problem.getNumberOne()));
             break;
          case 2:
             problem.setOperation(Operation.MULTIPLY);
             problem.setNumberOne(rand.nextInt(12));
             problem.setNumberTwo(rand.nextInt(12));
             break;
          case 3:
             problem.setOperation(Operation.DIVIDE);
             problem.setNumberOne(rand.nextInt(144));
             int numberTwo = rand.nextInt(problem.getNumberOne());
             while (problem.getNumberOne() / numberTwo % 2 != 0) {
                numberTwo = rand.nextInt(problem.getNumberOne());
             }
             problem.setNumberTwo(numberTwo);
             break;
      }
      
      return problem;
   }
   
   public int calculateAnswer(Problem problem) {
      int answer = -1;
      switch(problem.getOperation()) {
         case ADD:
            answer =  problem.getNumberOne() + problem.getNumberTwo();
            break;
         case SUBTRACT:
            answer =  problem.getNumberOne() - problem.getNumberTwo();
            break;
         case MULTIPLY:
            answer =  problem.getNumberOne() * problem.getNumberTwo();
            break;
         case DIVIDE:
            answer = problem.getNumberOne() / problem.getNumberTwo();
            break;
      }
      
      log.debug(problem.getNumberOne() + " " + problem.getOperation().name() + " " + problem.getNumberTwo() + " = " + answer);
      return answer;
   }   
}
