package com.amydegregorio.mathgame.dto;

public class AnswerResponse {
   private int correctAnswer;
   private int userAnswer;
   private boolean correct;
   
   public int getCorrectAnswer() {
      return correctAnswer;
   }
   
   public void setCorrectAnswer(int correctAnswer) {
      this.correctAnswer = correctAnswer;
   }
   
   public int getUserAnswer() {
      return userAnswer;
   }
   
   public void setUserAnswer(int userAnswer) {
      this.userAnswer = userAnswer;
   }
   
   public boolean isCorrect() {
      return correct;
   }
   
   public void setCorrect(boolean correct) {
      this.correct = correct;
   }
}
