package com.amydegregorio.mathgame.dto;

public class Problem {
   public enum Operation {
      ADD,
      SUBTRACT,
      MULTIPLY,
      DIVIDE
   }
    
   private int numberOne;
   private Operation operation;
   private int numberTwo;
   private int answer;
   private int userAnswer;
   
   public int getNumberOne() {
      return numberOne;
   }
   
   public void setNumberOne(int numberOne) {
      this.numberOne = numberOne;
   }
   
   public Operation getOperation() {
      return operation;
   }
   
   public void setOperation(Operation operation) {
      this.operation = operation;
   }
   
   public int getNumberTwo() {
      return numberTwo;
   }
   
   public void setNumberTwo(int numberTwo) {
      this.numberTwo = numberTwo;
   }

   public int getAnswer() {
      return answer;
   }

   public void setAnswer(int answer) {
      this.answer = answer;
   }

   public int getUserAnswer() {
      return userAnswer;
   }

   public void setUserAnswer(int userAnswer) {
      this.userAnswer = userAnswer;
   }
   
}
