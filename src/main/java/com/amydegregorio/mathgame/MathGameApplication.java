package com.amydegregorio.mathgame;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class MathGameApplication {
	@Value("${cors.allowedOrigin}")
	private String allowedOrigin;

	public static void main(String[] args) {
		SpringApplication.run(MathGameApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/v1/**").allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS").allowedOrigins(allowedOrigin);
			}
		};
	}
}
