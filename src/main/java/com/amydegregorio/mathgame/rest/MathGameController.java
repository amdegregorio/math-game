package com.amydegregorio.mathgame.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amydegregorio.mathgame.dto.AnswerResponse;
import com.amydegregorio.mathgame.dto.Problem;
import com.amydegregorio.mathgame.service.ProblemService;

@RestController
@RequestMapping("/api/v1/math")
public class MathGameController {
   Logger log = LoggerFactory.getLogger(MathGameController.class);
   
   @Autowired
   private ProblemService problemService;

   @GetMapping("/get-problem")
   public Problem getProblem() {
      return problemService.generateProblem();
   }
   
   @PutMapping("/check-answer")
   public AnswerResponse checkAnswer(@RequestBody Problem problem) {
      AnswerResponse response = new AnswerResponse();
      response.setUserAnswer(problem.getUserAnswer());
      int answer = problemService.calculateAnswer(problem);
      log.debug("Calculated Answer: " + answer);
      response.setCorrectAnswer(answer);
      response.setCorrect(response.getUserAnswer() == response.getCorrectAnswer());
      return response;
   }
}
