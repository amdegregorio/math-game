package com.amydegregorio.mathgame.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.reset;
import static org.mockito.BDDMockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.amydegregorio.mathgame.dto.Problem;
import com.amydegregorio.mathgame.dto.Problem.Operation;
import com.amydegregorio.mathgame.rest.MathGameController;
import com.amydegregorio.mathgame.service.ProblemService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(MathGameController.class)
public class MathGameControllerTest {
   Logger log = LoggerFactory.getLogger(MathGameControllerTest.class);

   @Autowired
   private MockMvc mvc;
   
   @MockBean
   private ProblemService service;
   
   @Autowired
   private ObjectMapper objectMapper;
   
   @Before
   public void setUp() throws Exception {
	   
   }
   
   @Test
   public void givenProblem_whenGetProblem_thenReturnJsonObject() throws Exception {
      Problem problem = new Problem();
      problem.setNumberOne(2);
      problem.setNumberTwo(4);
      problem.setOperation(Operation.MULTIPLY);
      
      given(service.generateProblem()).willReturn(problem);
      
      mvc.perform(get("/api/v1/math/get-problem")
         .contentType(MediaType.APPLICATION_JSON))
         .andExpect(status().isOk())
         .andExpect(jsonPath("$.numberOne").value(2))
         .andExpect(jsonPath("$.numberTwo").value(4))
         .andExpect(jsonPath("$.operation", is(Operation.MULTIPLY.name())));
      verify(service, VerificationModeFactory.times(1)).generateProblem();
      reset(service);
   }
   
   @Test
   public void givenProblemWithAnswer_whenCheckAnswer_thenReturnJsonObject() throws Exception {
      Problem problem = new Problem();
      problem.setNumberOne(2);
      problem.setNumberTwo(4);
      problem.setOperation(Operation.MULTIPLY);
      problem.setUserAnswer(problem.getNumberOne() * problem.getNumberTwo());
      problem.setAnswer(problem.getNumberOne() * problem.getNumberTwo());
      
      int answer = problem.getNumberOne() * problem.getNumberTwo();
      log.debug("Answer: " + answer);
      
      given(service.calculateAnswer(any())).willReturn(Integer.valueOf(answer)); 
      
      mvc.perform(put("/api/v1/math/check-answer")
         .contentType(MediaType.APPLICATION_JSON)
         .content(objectMapper.writeValueAsString(problem)))
         .andExpect(status().isOk())
         .andExpect(jsonPath("$.correctAnswer").value(answer))
         .andExpect(jsonPath("$.userAnswer").value(answer))
         .andExpect(jsonPath("$.correct").value(true));
      reset(service);
   }
}
