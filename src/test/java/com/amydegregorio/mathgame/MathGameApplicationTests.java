package com.amydegregorio.mathgame;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.amydegregorio.mathgame.rest.MathGameController;

@SpringBootTest
class MathGameApplicationTests {
   @Autowired
   private MathGameController mathGameController;

	@Test
	void contextLoads() {
	   assertNotNull(mathGameController);
	}

}
