package com.amydegregorio.mathgame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.amydegregorio.mathgame.dto.Problem;
import com.amydegregorio.mathgame.dto.Problem.Operation;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProblemServiceTest {
   
   @Autowired
   private ProblemService problemService;

   @Test
   public void whenGenerateProblem_ProblemReturned() {
      Problem problem = problemService.generateProblem();
      assertNotNull(problem);
      assertNotNull(problem.getOperation());
   }
   
   @Test
   public void givenAdditionProblem_whenCalculateAnswer_thenCorrectAnswerReturned() {
      Problem problem = new Problem();
      problem.setOperation(Operation.ADD);
      problem.setNumberOne(2);
      problem.setNumberTwo(4);
      int result = problemService.calculateAnswer(problem);
      assertEquals(2+4, result);
   }
   
   @Test
   public void givenSubtractionProblem_whenCalculateAnswer_thenCorrectAnswerReturned() {
      Problem problem = new Problem();
      problem.setOperation(Operation.SUBTRACT);
      problem.setNumberOne(8);
      problem.setNumberTwo(4);
      int result = problemService.calculateAnswer(problem);
      assertEquals(8-4, result);
   }
   
   @Test
   public void givenMultiplicatoinProblem_whenCalculateAnswer_thenCorrectAnswerReturned() {
      Problem problem = new Problem();
      problem.setOperation(Operation.MULTIPLY);
      problem.setNumberOne(20);
      problem.setNumberTwo(20);
      int result = problemService.calculateAnswer(problem);
      assertEquals(20*20, result);
   }
   
   @Test
   public void givenDivisionProblem_whenCalculateAnswer_thenCorrectAnswerReturned() {
      Problem problem = new Problem();
      problem.setOperation(Operation.DIVIDE);
      problem.setNumberOne(10);
      problem.setNumberTwo(5);
      int result = problemService.calculateAnswer(problem);
      assertEquals(10/5, result);
   }
}
